"""P2PRFD HTTP proxy"""

import cgi
import hashlib
import helper
import http_helper
import httplib
import md5
import random
import socket
import sys
import threading
import time
import urllib
import urlparse

import twisted.internet.reactor
import twisted.web.resource
import twisted.web.server

import entangled_network

class Observer:
    def http_get(self, queries):
        obs = self.obs_addr
        try:
            http_helper.http_get(obs, queries)
        except Exception, ex:
            print 'observer is off'
            print str(ex)
            pass
    def notify_new_root(self, feed_url, root):
        queries = {}
        queries['action'] = 'new_root'
        queries['rss'] = feed_url
        queries['root'] = repr(root)
        self.http_get(queries)
        pass
    def notify_adopt_child(self, feed_url, parent, child, loc):
        queries = {}
        queries['action'] = 'adopt_child'
        queries['rss'] = feed_url
        queries['parent'] = repr(parent)
        queries['child'] = repr(child)
        queries['loc'] = loc
        self.http_get(queries)
        pass
    def notify_update_received(self, node, publish_id, timestamp, level):
        queries = {}
        queries['action'] = 'update_received'
        queries['node'] = node
        queries['publish_id'] = publish_id
        queries['timestamp'] = timestamp
        queries['level'] = level
        self.http_get(queries)
    pass
class P2prfdProxy(twisted.web.resource.Resource):
    """HTTP request handler for P2PRFD proxy"""
    isLeaf = True
    cache_data = {} 
    parent_of = {}
    ip_addr = {}
    child_left = {}
    child_right = {}
    update_interval = 63
    root_update_interval = 60
    def generate_id(self):
        sh = hashlib.sha1()
        sh.update(str(random.random()))
        id = sh.hexdigest()[:7]
        return id
    def set_root(self, feed_url):
        root_key = 'root:' + feed_url
        me = self.get_me(feed_url)
        self.dht_network.set_value(root_key, repr(me))
        self.observer.notify_new_root(feed_url, me)
        pass
    def insert_new_feed(self, feed_url, request=None):
        print 'inserting new feed %s' % (feed_url)
        def root_gotten(root):
            data = ''
            if type(root) is str:
                root = eval(root)
                try:
                    have_new_parent = False
                    while not have_new_parent:
                        print 'getting parent'
                        me = (self.ip_addr[feed_url], self.p2prfd_http_port)
                        origin = self.get_me(feed_url)
                        parent = self.get_parent_from_root(root, feed_url)
                        print 'parent =', repr(parent)
                        ret = self.insert_to_parent(parent, feed_url)
                        print 'insert status =', repr(ret)
                        if not ret: print 'FAILED inserting'
                        have_new_parent = ret
                    self.parent_of[feed_url] = parent
                    data = self.get_data_from_parent(parent, feed_url)
                except Exception, ex:
                    print 'root error'
                    print str(ex)
                    data = self.get_data_from_server(feed_url)
                    self.set_root(feed_url)
                    #dht_network.set_value(root_key, repr((self.ip_addr[feed_url], self.p2prfd_http_port)))
            elif root is None:
                data = self.get_data_from_server(feed_url)

                """tell the DHT that I am the root of this feed"""
                self.set_root(feed_url)
                #dht_network.set_value(root_key, repr((self.ip_addr[feed_url], self.p2prfd_http_port)))
            else:
                data = 'should not contain this'
                pass
            if request:
                request.write(data)
                request.finish()
            """cache the data"""
            self.cache_data[feed_url] = data
        parsed = urlparse.urlparse(feed_url)
        host = parsed.netloc
        http_port = 80
        if parsed.port: http_port = parsed.port
        sock = socket.socket()
        sock.connect((host, http_port))
        ipaddr = sock.getsockname()[0]
        self.ip_addr[feed_url] = ipaddr

        dht_network = self.dht_network
        root_key = 'root:' + feed_url
        dht_network.get_value(root_key, root_gotten)
        pass
    def notify_node(self, node, feed_url, content, publish_id, level):
        return self.http_rpc(node, 'notify_update', feed_url, 'status', content, None, publish_id, level)
    def try_connection(self, try_actions, except_actions):
        try:
            try_actions()
        except Exception, exc:
            print 'exception:', exc
            except_actions()
    def notify_children(self, feed_url, publish_id=None, level=None):
        print 'notify children %s' % (feed_url)
        child_left = None
        child_right = None
        if feed_url in self.child_left: child_left = self.child_left[feed_url]
        if feed_url in self.child_right: child_right = self.child_right[feed_url]
        me = (self.ip_addr[feed_url], self.p2prfd_http_port)
        content = self.cache_data[feed_url]
        if child_left:
            def try_actions():
                self.notify_node(child_left, feed_url, content, publish_id, level)
            def except_actions():
                print 'left child %s error' % (repr(child_left))
                #child_left = None
                self.observer.notify_adopt_child(feed_url, self.get_me(feed_url), None, 'left')
                helper.del_key(self.child_left, feed_url)
            self.try_connection(try_actions, except_actions)
        if child_right:
            def try_actions():
                self.notify_node(child_right, feed_url, content, publish_id, level)
            def except_actions():
                print 'right child %s error' % (repr(child_right))
                #child_right = None
                self.observer.notify_adopt_child(feed_url, self.get_me(feed_url), None, 'right')
                helper.del_key(self.child_right, feed_url)
            self.try_connection(try_actions, except_actions)
        pass
    def update_feed(self, feed_url):
        if feed_url in self.parent_of:
            parent = self.parent_of[feed_url]
            try:
                data = self.get_data_from_parent(parent, feed_url)
                self.cache_data[feed_url] = data
            except:
                print 'parent %s error' % (repr(parent))
                self.cache_data[feed_url] = 'parent error'
                del self.parent_of[feed_url]
                self.insert_new_feed(feed_url)
        else:
            data = self.get_data_from_server(feed_url)
            self.cache_data[feed_url] = data
            publish_id = md5.new(str(time.time()) + 'publish_key').hexdigest()[:7]
            self.observer.notify_update_received(self.get_me(feed_url), publish_id, time.time(), 0)
            self.notify_children(feed_url, publish_id, 1)
            pass
        pass
    def ping_node(self, node, feed_url):
        return self.http_rpc(node, 'ping', feed_url, 'status')
    def check_children(self, feed_url):
        print 'checking children'
        child_left = None
        child_right = None
        if feed_url in self.child_left: child_left = self.child_left[feed_url]
        if feed_url in self.child_right: child_right = self.child_right[feed_url]
        me = (self.ip_addr[feed_url], self.p2prfd_http_port)
        if child_left:
            def try_actions():
                self.ping_node(child_left, feed_url)
            def except_actions():
                print 'left child %s error' % (repr(child_left))
                self.observer.notify_adopt_child(feed_url, self.get_me(feed_url), None, 'left')
                helper.del_key(self.child_left, feed_url)
            self.try_connection(try_actions, except_actions)
        if child_right:
            def try_actions():
                self.ping_node(child_right, feed_url)
            def except_actions():
                print 'right child %s error' % (repr(child_right))
                self.observer.notify_adopt_child(feed_url, self.get_me(feed_url), None, 'right')
                helper.del_key(self.child_right, feed_url)
            self.try_connection(try_actions, except_actions)
        #print 'check result:'
        #print 'left child is %s' % (repr(child_left))
        #print 'right child is %s' % (repr(child_right))
        pass
    def root_update(self):
        print 'root updating'
        for feed_url in self.cache_data:
            if not (feed_url in self.parent_of):
                print 'updating %s' % (feed_url)
                twisted.internet.reactor.callInThread(self.update_feed, feed_url)
                twisted.internet.reactor.callInThread(self.check_children, feed_url)
        twisted.internet.reactor.callLater(self.root_update_interval, self.root_update)
    def update(self):
        print 'non-root updating'
        for feed_url in self.cache_data:
            if (feed_url in self.parent_of):
                print 'updating %s' % (feed_url)
                twisted.internet.reactor.callInThread(self.update_feed, feed_url)
                twisted.internet.reactor.callInThread(self.check_children, feed_url)
        twisted.internet.reactor.callLater(self.update_interval, self.update)
        pass
    def http_get(self, target, queries):
        http_con = httplib.HTTPConnection(target[0], target[1])
        http_con.request("GET", '/?' + urllib.urlencode(queries))
        response = http_con.getresponse()
        resp_str = response.read()
        result = eval(resp_str)
        http_con.close()
        return result
    def get_data_from_parent(self, parent, key):
        print 'fetching from parent %s' % (repr(parent))
        return self.http_rpc(parent, 'get_rss', key, 'data')
    def get_data_from_server(self, addr):
        """get a resource directly from addr"""
        parse_result = urlparse.urlparse(addr)
        print 'fetching directly from %s' % (addr)
        http_con = httplib.HTTPConnection(parse_result.netloc)
        path = parse_result.path
        if parse_result.query:
            path += '?' + parse_result.query
        http_con.request("GET", path)
        response = http_con.getresponse()
        data = response.read()
        http_con.close()
        return data
    def get_parent_from_root(self, root, feed_url, origin=None):
        """find a good parent from this root"""
        return self.http_rpc(root, 'get_parent', feed_url, 'parent', None, origin)
    def http_rpc(self, target, action, feed_url, ret_key, data=None, origin=None, publish_id=None, level=None):
        queries = {}
        queries['action'] = action
        queries['rss'] = feed_url
        queries['node_id'] = self.node_id
        queries['from'] = (self.ip_addr[feed_url], self.p2prfd_http_port, self.node_id)
        queries['origin'] = origin
        queries['publish_id'] = publish_id
        queries['level'] = level
        if origin is None: queries['origin'] = queries['from']
        #me = (self.ip_addr[feed_url], self.p2prfd_http_port)
        me = self.get_me(feed_url)
        queries['addr'] = repr(me)
        if data is None: 
            result = http_helper.http_get(target, queries)
        else:
            queries['data'] = data
            result = http_helper.http_post(target, queries)
        return result[ret_key]

    def insert_to_parent(self, parent, feed_url):
        print 'insert_to_parent %s' % (repr(parent))
        ret = self.http_rpc(parent, 'insert_child', feed_url, 'status')
        return ret
    def get_me(self, feed_url):
        me = (self.ip_addr[feed_url], self.p2prfd_http_port, self.node_id)
        return me
    def render_GET(self, request):
        """beginning of processing the request"""
        """if I have the data in cache"""
        if (request.uri in self.cache_data):
            data = self.cache_data[request.uri]
            print 'url %s found in cache' % request.uri
            return data
        else:
            self.insert_new_feed(request.uri, request)
            return twisted.web.server.NOT_DONE_YET
class P2prfdResource(twisted.web.resource.Resource):
    isLeaf = True
    def get_node_info(self, node, feed_url):
        return self.rsrc.http_rpc(node, 'get_node_info', feed_url, 'info')
    def render_POST(self, request):
        queries = request.args
        action = ''
        if 'action' in queries: action = queries['action'][0]
        result = {}
        result['status'] = False
        result['data'] = 'no data found'
        #print 'AWAL ACTION %s' % action
        if action == 'notify_update':
            feed_url = queries['rss'][0]
            print 'received update for %s' % feed_url
            content = queries['data'][0]
            self.rsrc.cache_data[feed_url] = content
            publish_id = queries['publish_id'][0]
            level = int(queries['level'][0])
            if publish_id != 'None': 
                self.rsrc.observer.notify_update_received(self.rsrc.get_me(feed_url), publish_id, time.time(), level)
                pass
            level = int(queries['level'][0])
            twisted.internet.reactor.callInThread(self.rsrc.notify_children, feed_url, publish_id, level+1)
            result['status'] = True
        #print 'AKHIR ACTION %s' % action
        return repr(result)
    def render_GET(self, request):
        parsed = urlparse.urlparse(request.uri)
        queries = cgi.parse_qs(parsed.query)
        #print 'received queries =',  queries
        action = ''
        if 'action' in queries: action = queries['action'][0]
        result = {}
        result['status'] = False
        result['data'] = 'no data found'
        #print 'AWAL ACTION %s' % action
        if action == 'get_rss':
            feed_url = queries['rss'][0]
            if feed_url in self.rsrc.cache_data:
                result['status'] = True
                result['data'] = self.rsrc.cache_data[feed_url]
        elif action == 'get_parent':
            feed_url = queries['rss'][0]
            child_left = None
            child_right = None
            if feed_url in self.rsrc.child_left: child_left = self.rsrc.child_left[feed_url]
            if feed_url in self.rsrc.child_right: child_right = self.rsrc.child_right[feed_url]
            #me = (self.rsrc.ip_addr[feed_url], self.rsrc.p2prfd_http_port)
            me = self.rsrc.get_me(feed_url)
            from_node = eval(queries['from'][0])
            origin = eval(queries['origin'][0])
            #print 'DEBUG:'
            #print 'child_left =', repr(child_left)
            #print 'child_right =', repr(child_right)
            #print 'from =', repr(from_node)
            #print 'origin =', repr(origin)
            if (child_left == origin) or (child_right == origin):
                print 'ONE OF CHILD IS FOUND TO BE THE SAME AS GET_PARENT ASKER'
                parent = me
                result['status'] = True
                result['parent'] = parent
            else:
                if child_left:
                    try:
                        left_info = self.get_node_info(child_left, feed_url)
                    except Exception, exc:
                        print 'exceptione:', exc
                        print 'left child %s error' % (repr(child_left))
                        child_left = None
                        self.rsrc.observer.notify_adopt_child(feed_url, self.rsrc.get_me(feed_url), None, 'left')
                        helper.del_key(self.rsrc.child_left, feed_url)
                    pass
                if child_right:
                    try:
                        right_info = self.get_node_info(child_right, feed_url)
                    except Exception, exc:
                        print 'exceptione:', exc
                        print 'right child %s error' % (repr(child_right))
                        child_right = None
                        self.rsrc.observer.notify_adopt_child(feed_url, self.rsrc.get_me(feed_url), None, 'right')
                        helper.del_key(self.rsrc.child_right, feed_url)
                    pass
                if child_left and child_right:
                    if left_info['min_depth'] <= right_info['min_depth']:
                        parent = self.rsrc.get_parent_from_root(child_left, feed_url, origin)
                    else:
                        parent = self.rsrc.get_parent_from_root(child_right, feed_url, origin)
                    result['status'] = True
                    result['parent'] = parent
                else:
                    parent = me
                    result['status'] = True
                    result['parent'] = parent
            #result['parent'] = (self.rsrc.ip_addr[feed_url], self.rsrc.p2prfd_http_port)
            print 'left child is %s' % (repr(child_left))
            print 'right child is %s' % (repr(child_right))
            pass
        elif action == 'get_node_info':
            feed_url = queries['rss'][0]
            child_left = None
            child_right = None
            if feed_url in self.rsrc.child_left: child_left = self.rsrc.child_left[feed_url]
            if feed_url in self.rsrc.child_right: child_right = self.rsrc.child_right[feed_url]
            left_depth = 0
            right_depth = 0
            left_min_depth = 0
            right_min_depth = 0
            if child_left:
                left_info = self.get_node_info(child_left, feed_url)
                left_depth = left_info['depth']
                left_min_depth = left_info['min_depth']
                pass
            if child_right:
                right_info = self.get_node_info(child_right, feed_url)
                right_depth = right_info['depth']
                right_min_depth = right_info['min_depth']
                pass
            depth = max(left_depth, right_depth) + 1
            min_depth = min(left_min_depth, right_min_depth) + 1
            info = {}
            info['depth'] = depth
            info['min_depth'] = min_depth
            print 'depth is %s' % (depth)
            result['status'] = True
            result['info'] = info
        elif action == 'insert_child':
            node_id = eval(queries['from'][0])[2]
            if node_id == self.rsrc.node_id:
                print 'error connecting, try a moment later'
                sys.exit()
                pass
            else:
                feed_url = queries['rss'][0]
                child_left = None
                child_right = None
                if feed_url in self.rsrc.child_left: child_left = self.rsrc.child_left[feed_url]
                if feed_url in self.rsrc.child_right: child_right = self.rsrc.child_right[feed_url]
                from_node = eval(queries['from'][0])
                if (child_left == from_node):
                    result['status'] = True
                elif (child_right == from_node):
                    result['status'] = True
                else:
                    feed_url = queries['rss'][0]
                    addr = eval(queries['from'][0])
                    print 'inserting child %s' % (repr(addr))
                    if not (feed_url in self.rsrc.child_left):
                        self.rsrc.child_left[feed_url] = addr
                        parent = self.rsrc.get_me(feed_url)
                        child = addr
                        self.rsrc.observer.notify_adopt_child(feed_url, parent, child, 'left')
                        result['status'] = True
                    elif not (feed_url in self.rsrc.child_right):
                        self.rsrc.child_right[feed_url] = addr
                        parent = self.rsrc.get_me(feed_url)
                        child = addr
                        self.rsrc.observer.notify_adopt_child(feed_url, parent, child, 'right')
                        result['status'] = True
                    else:
                        result['status'] = False
                        pass
                    pass
        elif action == 'ping':
            result['status'] = True
            result['node_id'] = self.rsrc.node_id
        else:
            result['status'] = False
        #print 'AKHIR ACTION %s' % action
        return repr(result)
rsrc = P2prfdProxy()
obs_ip = ''
if len(sys.argv) == 7:
    known_node = (sys.argv[4], int(sys.argv[5]))
    obs_ip = sys.argv[6]
else:
    known_node = None
    obs_ip = sys.argv[4]
rsrc.dht_network = entangled_network.EntangledNetwork(int(sys.argv[1]), known_node)
rsrc.p2prfd_http_port = int(sys.argv[3])
rsrc.node_id = rsrc.generate_id()
rsrc.observer = Observer()
rsrc.observer.obs_addr = (obs_ip, 9501)
site = twisted.web.server.Site(rsrc)
p2prfd_rsrc = P2prfdResource()
p2prfd_rsrc.rsrc = rsrc
p2prfd_site = twisted.web.server.Site(p2prfd_rsrc)
update_timer = twisted.internet.reactor.callLater(rsrc.update_interval, rsrc.update)
root_update_timer = twisted.internet.reactor.callLater(rsrc.root_update_interval, rsrc.root_update)
rsrc.update_timer = update_timer
twisted.internet.reactor.listenTCP(int(sys.argv[2]), site)
twisted.internet.reactor.listenTCP(rsrc.p2prfd_http_port, p2prfd_site)
twisted.internet.reactor.run()
