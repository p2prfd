"""P2PRFD HTTP proxy"""

import cgi
import httplib
import socket
import sys
import threading
import urllib
import urlparse

import twisted.internet.reactor
import twisted.web.resource
import twisted.web.server

import entangled_network

class P2prfdProxy(twisted.web.resource.Resource):
    """HTTP request handler for P2PRFD proxy"""
    isLeaf = True
    cache_data = {} 
    parent_of = {}
    ip_addr = {}
    child_left = {}
    child_right = {}
    update_interval = 10
    def insert_new_feed(self, feed_url, request=None):
        print 'inserting new feed %s' % (feed_url)
        def root_gotten(root):
            data = ''
            if type(root) is str:
                root = eval(root)
                try:
                    parent = self.get_parent_from_root(root, feed_url)
                    me = (self.ip_addr[feed_url], self.p2prfd_http_port)
                    self.insert_to_parent(parent, me, feed_url)
                    self.parent_of[feed_url] = parent
                    data = self.get_data_from_parent(parent, feed_url)
                except:
                    data = self.get_data_from_server(feed_url)
                    dht_network.set_value(root_key, repr((self.ip_addr[feed_url], self.p2prfd_http_port)))
            elif root is None:
                data = self.get_data_from_server(feed_url)

                """tell the DHT that I am the root of this feed"""
                dht_network.set_value(root_key, repr((self.ip_addr[feed_url], self.p2prfd_http_port)))
            else:
                data = 'should not contain this'
                pass
            if request:
                request.write(data)
                request.finish()
            """cache the data"""
            self.cache_data[feed_url] = data
        parsed = urlparse.urlparse(feed_url)
        host = parsed.netloc
        http_port = 80
        if parsed.port: http_port = parsed.port
        sock = socket.socket()
        sock.connect((host, http_port))
        ipaddr = sock.getsockname()[0]
        self.ip_addr[feed_url] = ipaddr

        dht_network = self.dht_network
        root_key = 'root:' + feed_url
        dht_network.get_value(root_key, root_gotten)
    def update_feed(self, feed_url):
        if feed_url in self.parent_of:
            parent = self.parent_of[feed_url]
            try:
                data = self.get_data_from_parent(parent, feed_url)
                self.cache_data[feed_url] = data
            except:
                print 'parent %s error' % (repr(parent))
                self.cache_data[feed_url] = 'parent error'
                del self.parent_of[feed_url]
                self.insert_new_feed(feed_url)
        else:
            data = self.get_data_from_server(feed_url)
            self.cache_data[feed_url] = data
            pass
    def update(self):
        print 'updating'
        for feed_url in self.cache_data:
            print 'updating %s' % (feed_url)
            self.update_feed(feed_url)
        twisted.internet.reactor.callLater(self.update_interval, self.update)
        #self.update_timer.reset(self.update_interval)
    def get_data_from_parent(self, parent, key):
        #return 'url %s gotten from %s' % (key, repr(parent))
        """get data from parent"""
        print 'fetching from parent %s' % (repr(parent))
        http_con = httplib.HTTPConnection(parent[0], parent[1])
        queries = {}
        queries['action'] = 'get_rss'
        queries['rss'] = key
        http_con.request("GET", '/?' + urllib.urlencode(queries))
        response = http_con.getresponse()
        resp_str = response.read()
        result = eval(resp_str)
        http_con.close()
        return result['data']
    def get_data_from_server(self, addr):
        """get a resource directly from addr"""
        parse_result = urlparse.urlparse(addr)
        print 'fetching directly from %s' % (addr)
        http_con = httplib.HTTPConnection(parse_result.netloc)
        path = parse_result.path
        if parse_result.query:
            path += '?' + parse_result.query
        http_con.request("GET", path)
        response = http_con.getresponse()
        data = response.read()
        http_con.close()
        return data
    def get_parent_from_root(self, root, feed_url):
        """find a good parent from this root"""
        http_con = httplib.HTTPConnection(root[0], root[1])
        queries = {}
        queries['action'] = 'get_parent'
        queries['rss'] = feed_url
        http_con.request("GET", '/?' + urllib.urlencode(queries))
        response = http_con.getresponse()
        resp_str = response.read()
        result = eval(resp_str)
        parent = result['parent']
        return parent
    def insert_to_parent(self, parent, me, feed_url):
        http_con = httplib.HTTPConnection(parent[0], parent[1])
        queries = {}
        queries['action'] = 'insert_child'
        queries['rss'] = feed_url
        queries['addr'] = repr(me)
        http_con.request("GET", '/?' + urllib.urlencode(queries))
        response = http_con.getresponse()
        resp_str = response.read()
        result = eval(resp_str)
        return result['status']
    def render_GET(self, request):
        """beginning of processing the request"""
        """if I have the data in cache"""
        if (request.uri in self.cache_data):
            data = self.cache_data[request.uri]
            print 'url %s found in cache' % request.uri
            return data
        else:
            self.insert_new_feed(request.uri, request)
            return twisted.web.server.NOT_DONE_YET
class P2prfdResource(twisted.web.resource.Resource):
    isLeaf = True
    def get_node_info(self, node, feed_url):
        http_con = httplib.HTTPConnection(node[0], node[1])
        queries = {}
        queries['action'] = 'get_node_info'
        queries['rss'] = feed_url
        http_con.request("GET", '/?' + urllib.urlencode(queries))
        response = http_con.getresponse()
        resp_str = response.read()
        result = eval(resp_str)
        return result['info']
    def render_GET(self, request):
        parsed = urlparse.urlparse(request.uri)
        queries = cgi.parse_qs(parsed.query)
        action = queries['action'][0]
        result = {}
        result['status'] = False
        result['data'] = 'no data found'
        print 'action is %s' % (action)
        if action == 'get_rss':
            feed_url = queries['rss'][0]
            if feed_url in self.rsrc.cache_data:
                result['status'] = True
                result['data'] = self.rsrc.cache_data[feed_url]
        elif action == 'get_parent':
            feed_url = queries['rss'][0]
            child_left = None
            child_right = None
            if feed_url in self.rsrc.child_left: child_left = self.rsrc.child_left[feed_url]
            if feed_url in self.rsrc.child_right: child_right = self.rsrc.child_right[feed_url]
            me = (self.rsrc.ip_addr[feed_url], self.rsrc.p2prfd_http_port)
            if child_left:
                try:
                    left_info = self.get_node_info(child_left, feed_url)
                except:
                    print 'left child %s error' % (repr(child_left))
                    child_left = None
                    del self.rsrc.child_left[feed_url]
                pass
            if child_right:
                try:
                    right_info = self.get_node_info(child_right, feed_url)
                except:
                    print 'right child %s error' % (repr(child_right))
                    child_right = None
                    del self.rsrc.child_right[feed_url]
                pass
            if child_left and child_right:
                if left_info['depth'] <= right_info['depth']:
                    parent = self.rsrc.get_parent_from_root(child_left, feed_url)
                else:
                    parent = self.rsrc.get_parent_from_root(child_right, feed_url)
                result['status'] = True
                result['parent'] = parent
            else:
                parent = me
                result['status'] = True
                result['parent'] = parent
            #result['parent'] = (self.rsrc.ip_addr[feed_url], self.rsrc.p2prfd_http_port)
            print 'left child is %s' % (repr(child_left))
            print 'right child is %s' % (repr(child_right))
            pass
        elif action == 'get_node_info':
            feed_url = queries['rss'][0]
            child_left = None
            child_right = None
            if feed_url in self.rsrc.child_left: child_left = self.rsrc.child_left[feed_url]
            if feed_url in self.rsrc.child_right: child_right = self.rsrc.child_right[feed_url]
            left_depth = 0
            right_depth = 0
            if child_left:
                left_info = self.get_node_info(child_left, feed_url)
                left_depth = left_info['depth']
                pass
            if child_right:
                right_info = self.get_node_info(child_right, feed_url)
                right_depth = right_info['depth']
                pass
            depth = max(left_depth, right_depth) + 1
            info = {}
            info['depth'] = depth
            print 'depth is %s' % (depth)
            result['status'] = True
            result['info'] = info
        elif action == 'insert_child':
            feed_url = queries['rss'][0]
            addr = eval(queries['addr'][0])
            print 'inserting child %s' % (repr(addr))
            if not (feed_url in self.rsrc.child_left):
                self.rsrc.child_left[feed_url] = addr
                result['status'] = True
            elif not (feed_url in self.rsrc.child_right):
                self.rsrc.child_right[feed_url] = addr
                result['status'] = True
            else:
                result['status'] = False
        else:
            result['status'] = False
        return repr(result)
rsrc = P2prfdProxy()
if len(sys.argv) == 6:
    known_node = (sys.argv[4], int(sys.argv[5]))
else:
    known_node = None
rsrc.dht_network = entangled_network.EntangledNetwork(int(sys.argv[1]), known_node)
rsrc.p2prfd_http_port = int(sys.argv[3])
site = twisted.web.server.Site(rsrc)
p2prfd_rsrc = P2prfdResource()
p2prfd_rsrc.rsrc = rsrc
p2prfd_site = twisted.web.server.Site(p2prfd_rsrc)
update_timer = twisted.internet.reactor.callLater(rsrc.update_interval, rsrc.update)
rsrc.update_timer = update_timer
twisted.internet.reactor.callLater(2, rsrc.dht_network.delete_key, 'root:http://localhost/coba.txt')
twisted.internet.reactor.listenTCP(int(sys.argv[2]), site)
twisted.internet.reactor.listenTCP(rsrc.p2prfd_http_port, p2prfd_site)
twisted.internet.reactor.run()
