import cgi
import hashlib
import http_helper
import httplib
import random
import socket
import sys
import threading
import urllib
import urlparse

import twisted.internet.reactor
import twisted.web.resource
import twisted.web.server

import entangled_network

class ObserverResource(twisted.web.resource.Resource):
    isLeaf = True
    trees = {}
    last_name = 0
    names = {}
    publish_time = {}
    delay_count = 0
    average_delay = 0
    graph_counter = 0
    def __init__(self):
        self.delay_log_file = open('delays.log', 'w')
    def produce_graph(self, rss):
        f = open('graph/graph%03d.dot' % self.graph_counter, 'w')
        f.write(self.tree_pic_dot(self.trees[rss]))
        f.close()
        self.graph_counter += 1
    def node_pic(self, node, tree):
        if node:
            name = self.names[node]
            retu = name
            left_node = None
            right_node = None
            if node in tree['child_left']: left_node = tree['child_left'][node]
            if node in tree['child_right']: right_node = tree['child_right'][node]
            left_pic = self.node_pic(left_node, tree)
            right_pic = self.node_pic(right_node, tree)
            lefts = left_pic.split('\n')
            retu += '-' + lefts[0] + '\n'
            for line in lefts[1:]:
                retu += '  |' + line + '\n'
                pass
            rights = right_pic.split('\n')
            retu += '  -' + rights[0] + '\n'
            for line in rights[1:]:
                retu += '   ' + line + '\n'
                pass
            return retu
        else:
            return '00'
        pass
    def node_dot(self, node, tree):
        retu = ''
        if node in tree['child_left'] and tree['child_left'][node]:
            left_node = tree['child_left'][node]
            retu += self.names[node] + ' -> ' + self.names[left_node] + ';\n'
            retu += self.node_dot(left_node, tree)
        if node in tree['child_right'] and tree['child_right'][node]:
            right_node = tree['child_right'][node]
            retu += self.names[node] + ' -> ' + self.names[right_node] + ';\n'
            retu += self.node_dot(right_node, tree)
        return retu
    def tree_pic_dot(self, tree):
        root = tree['root']
        retu = 'digraph p2prfd {\n'
        retu += self.names[root] + ';\n'
        retu += self.node_dot(root, tree)
        retu += '}\n'
        return retu
        pass
    def tree_pic(self, tree):
        root = tree['root']
        return self.node_pic(root, tree)
        pass
    def add_name(self, node):
        if node and not (node in self.names):
            self.last_name += 1
            self.names[node] = '%02d' % self.last_name
            pass
    def render_GET(self, request):
        queries = request.args
        request.responseHeaders.setRawHeaders('content-type', ['text/plain'])
        print 'queries = %s' % repr(queries)
        action = None
        if 'action' in queries: action = queries['action'][0]
        if action == 'new_root':
            feed_url = queries['rss'][0]
            root = eval(queries['root'][0])
            self.add_name(root)
            if feed_url in self.trees:
                pass
            else:
                self.trees[feed_url] = {'root': None, 'child_left': {}, 'child_right': {}}
            self.trees[feed_url]['root'] = root
            self.produce_graph(feed_url)
            pass
        elif action == 'adopt_child':
            parent = eval(queries['parent'][0]);
            child = eval(queries['child'][0]);
            feed_url = queries['rss'][0]
            loc = queries['loc'][0]
            self.add_name(parent)
            self.add_name(child)
            if loc == 'left':
                self.trees[feed_url]['child_left'][parent] = child
                pass
            elif loc == 'right':
                self.trees[feed_url]['child_right'][parent] = child
                pass
            self.produce_graph(feed_url)
            pass
        elif action == 'update_received':
            print 'publish_id =', queries['publish_id'][0]
            print 'timestamp =', queries['timestamp'][0]
            publish_id = queries['publish_id'][0]
            timestamp = queries['timestamp'][0]
            if publish_id not in self.publish_time:
                self.publish_time[publish_id] = float(timestamp)
            else:
                update_delay = float(timestamp) - self.publish_time[publish_id]
                self.average_delay = (self.average_delay*self.delay_count + update_delay) / (self.delay_count+1)
                self.delay_count = self.delay_count+1
                print 'update_delay = %.2f' % update_delay
                print 'average_delay = %.5f' % self.average_delay
                level = int(queries['level'][0])
                node = eval(queries['node'][0])
                name = self.names[node]
                self.delay_log_file.write('update delay = %.2f (node %s, level %d)\n' % (update_delay, name, level))
                self.delay_log_file.write('average delay = %.5f\n' % self.average_delay)
            pass
        else:
            rss = None
            if 'rss' in queries: rss = queries['rss'][0]
            if rss: 
                if rss in self.trees: 
                    if 'type' in queries:
                        return self.tree_pic_dot(self.trees[rss])
                    else:
                        return self.tree_pic(self.trees[rss])
                else: return 'tree for %s not found' % rss
            pass
        result = {}
        return repr(result)
    pass
rsrc = ObserverResource()
site = twisted.web.server.Site(rsrc)
twisted.internet.reactor.listenTCP(9501, site)
twisted.internet.reactor.run()
