"""helper functions related to HTTP"""

import httplib
import urllib

def http_get(target, queries):
    #print 'AWAL MINTA GET ke %s queries %s' % (repr(target), repr(queries))
    #print 'before get connection'
    http_con = httplib.HTTPConnection(target[0], target[1])
    #print 'after get connection'
    #print 'before get request'
    http_con.request("GET", '/?' + urllib.urlencode(queries))
    #print 'after get request'
    #print 'before get getresponse'
    response = http_con.getresponse()
    #print 'after get getresponse'
    resp_str = response.read()
    result = eval(resp_str)
    http_con.close()
    #print 'AKHIR MINTA GET ke %s queries %s' % (repr(target), repr(queries))
    return result
def http_post(target, queries):
    #print 'AWAL MINTA POST ke %s queries %s' % (repr(target), repr(queries))
    #print 'before post connection'
    http_con = httplib.HTTPConnection(target[0], target[1])
    #print 'after post connection'
    headers = {"Content-type": "application/x-www-form-urlencoded", "Accept": "text/plain"}
    body = urllib.urlencode(queries)
    #print 'before post request'
    http_con.request("POST", '/', body, headers)
    #print 'after post request'
    #print 'before post getresponse'
    response = http_con.getresponse()
    #print 'after post getresponse'
    resp_str = response.read()
    result = eval(resp_str)
    http_con.close()
    #print 'AKHIR MINTA POST ke %s queries %s' % (repr(target), repr(queries))
    return result
