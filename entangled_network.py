import hashlib

import entangled.kademlia.protocol
import entangled.node

class EntangledNetwork:
    """a class representing an Entangled network"""
    def __init__(self, udp_port, known_node = None):
        self.node = entangled.node.EntangledNode(udp_port)
        if known_node is not None:
            known_nodes = [known_node]
        else:
            known_nodes = []
        print 'joining %s' % (known_nodes)
        self.node.joinNetwork(known_nodes)
        pass
    def delete_key(self, url):
        def success_cb(result):
            print 'success deleting'
            pass
        def error_cb(failure):
            print 'error deleting'
            pass
        sh = hashlib.sha1()
        sh.update(url)
        hex_key = sh.hexdigest()
        print 'deleting key %s from DHT' % (url)
        h_key = sh.digest()
        df = self.node.iterativeDelete(h_key)
        df.addCallback(success_cb)
        df.addErrback(error_cb)
    def set_value(self, key, value):
        sh = hashlib.sha1()
        sh.update(key)
        hex_key = sh.hexdigest()
        print 'inserting key %s into DHT' % (key)
        h_key = sh.digest()
        df = self.node.iterativeStore(h_key, value)
        def set_success(result):
            print 'completed inserting key %s into DHT' % (key)
        def set_failed(failure):
            #failure.trap(entangled.kademlia.protocol.TimeoutError)
            print 'failed inserting key %s into DHT' % (key)
        df.addCallback(set_success)
        df.addErrback(set_failed)
    def get_value(self, key, callback):
        sh = hashlib.sha1()
        sh.update(key)
        h_key = sh.digest()
        hex_key = sh.hexdigest()
        print 'retrieving value of key %s from DHT' % (key)
        self.found = False
        def returning(result):
            if type(result) == dict:
                print 'DHT response: key %s found' % key
                #print 'value retrieved is %s' % (repr(result[h_key]))
                callback(result[h_key])
            else:
                print 'DHT response: key %s not found' % key
                callback(None)
            self.found = result
        def error_returning(failure):
            print 'DHT error'
            print repr(failure)
            print str(failure)
            #failure.trap(Exception)
            self.found = 'error lho'
        df = self.node.iterativeFindValue(h_key)
        df.addCallback(returning)
        df.addErrback(error_returning)
